const mainItems = document.querySelectorAll('.main_item');

mainItems.forEach((mainItem) => {
    mainItem.addEventListener('click', () => {
        mainItem.classList.toggle(
            'main_item--open'
        );
    })
});